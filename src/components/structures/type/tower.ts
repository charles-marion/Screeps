/**
 * Initialization scripts for CreepManager module.
 *
 * @export
 * @param {Room} room
 */
export class TowerStructure {
    public static run(room: Room): void {
        let towers = <StructureTower[]> room.find(FIND_MY_STRUCTURES, {
            filter: { structureType: STRUCTURE_TOWER }
        });
        for (let tower of towers) {
            let closestDamagedStructure = <Structure> tower.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (structure: Structure) => structure.hits < structure.hitsMax
            });
            if (closestDamagedStructure) {
                tower.repair(closestDamagedStructure);
            }

            let closestHostile = <Creep> tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
            if (closestHostile) {
                tower.attack(closestHostile);
            }
        }

    }
}
