import { Walls } from './type/wall';

export class StructureManager {
    /**
     * Initialization scripts for StructureManager module.
     *
     * @export
     * @param {Room} room
     */
    public static run(room: Room): void {
        const level = room.controller.level;
        if (Object.keys(Game.constructionSites).length === 100) {
            return;
        }
        // Add extensions
        const expectedExtension = CONTROLLER_STRUCTURES.extension[level];
        const extensions = room.find(FIND_MY_STRUCTURES, {
            filter: {structureType: STRUCTURE_EXTENSION}
        });

        if (extensions.length < expectedExtension) {
            const x = 9 + extensions.length % 5;
            const y = 23 - Math.floor(extensions.length / 5);
            room.createConstructionSite(x, y, STRUCTURE_EXTENSION);
        }

        // Add towers
        const towers = room.find(FIND_MY_STRUCTURES, {
            filter: {structureType: STRUCTURE_TOWER}
        });
        if (towers.length === 0 && level > 2) {
            room.createConstructionSite(20, 22, STRUCTURE_TOWER);
        }

        // Add walls
        if (level > 1) {
            Walls.addWalls(room);
        }
    }
}