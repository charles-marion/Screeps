import { Common } from './common';
export class Upgrader extends Common {
    public static run(creep: Creep) {
        if (creep.memory.task) {
            const target = <StructureController> creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter(object: Structure) {
                    return object.structureType === STRUCTURE_CONTROLLER;
                }
            });
            if (creep.upgradeController(target) === ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
                Common.checkRoad(creep);
            } else if (creep.upgradeController(target) === ERR_NOT_ENOUGH_RESOURCES) {
                creep.memory.task = false;
            }
        } else {
            const target = <Source> creep.pos.findClosestByRange(FIND_SOURCES_ACTIVE);
            if (creep.harvest(target) === ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
            }

            if (_.sum(creep.carry) === creep.carryCapacity) {
                creep.memory.task = true;
            }

        }
    }
}