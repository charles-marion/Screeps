import { Upgrader } from './upgrader';
import { Common } from './common';
export class Builder extends Common {
    public static run(creep: Creep) {
        if (creep.memory.task) {

            // We do roads last
            let target = <ConstructionSite> creep.pos.findClosestByPath(FIND_CONSTRUCTION_SITES, {
                filter(object: ConstructionSite) {
                    return object.structureType !== STRUCTURE_ROAD;
                }
            });
            if (!target) {
                target = creep.pos.findClosestByPath(FIND_CONSTRUCTION_SITES);
            }
            if (!target) {
                // idle
                Upgrader.run(creep);
                return;
            }

            if (creep.build(target) === ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
            } else if (creep.build(target) === ERR_NOT_ENOUGH_RESOURCES) {
                creep.memory.task = false;
            }
        } else {
            let target = <Source> creep.pos.findClosestByRange(FIND_SOURCES_ACTIVE);
            target = Game.getObjectById('5982fcabb097071b4adbde8b');
            if (creep.harvest(target) === ERR_NOT_IN_RANGE) {
                creep.moveTo(target);

            }

            if (_.sum(creep.carry) === creep.carryCapacity) {
                creep.memory.task = true;
            }
        }
    }
}