export class Common {
    public static checkRoad(creep: Creep) {
        if (Object.keys(Game.constructionSites).length === 100) {
            return;
        }
        const look = creep.pos.look();
        let roadFound = false;
        look.forEach((lookObject: LookAtResult) => {
            if (lookObject.type === LOOK_STRUCTURES &&
                lookObject[LOOK_STRUCTURES].structureType === STRUCTURE_ROAD) {
                roadFound = true;
            }
        });

        if (!roadFound) {
            creep.room.createConstructionSite(creep.pos.x, creep.pos.y, STRUCTURE_ROAD);
        }
    }
}