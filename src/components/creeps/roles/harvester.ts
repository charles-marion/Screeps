import { Upgrader } from './upgrader';
import { Common } from './common';
export class Harverter extends Common {
    public static run(creep: Creep) {
        if (creep.memory.task) {
            let target = <Structure> creep.pos.findClosestByPath(FIND_STRUCTURES, { filter(s: StructureExtension | StructureSpawn | StructureTower) {
                return  s.energy !== s.energyCapacity &&
                    (s.structureType === STRUCTURE_EXTENSION || s.structureType === STRUCTURE_SPAWN
                    || s.structureType === STRUCTURE_TOWER);
            }});

            if (!target) {
                // idle
                Upgrader.run(creep);
                return;
            } else if (creep.transfer(target, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
                Common.checkRoad(creep);
            }

            if (_.sum(creep.carry) === 0) {
                creep.memory.task = false;
            }
        } else {
            const target = <Source> creep.pos.findClosestByRange(FIND_SOURCES_ACTIVE);
            if (creep.harvest(target) === ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
            }
            if (_.sum(creep.carry) === creep.carryCapacity) {
                creep.memory.task = true;
            }
        }
    }
}