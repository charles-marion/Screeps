import * as Config from '../../config/config';

import { log } from '../../lib/logger/log';
import { Harverter } from './roles/harvester';
import { Upgrader } from './roles/upgrader';
import { Builder } from './roles/builder';

export class CreepManager {
    /**
     * Initialization scripts for CreepManager module.
     *
     * @export
     * @param {Room} room
     */
    public static run(room: Room): void {
        const creeps = room.find<Creep>(FIND_MY_CREEPS);
        const creepCount = _.size(creeps);

        if (Config.ENABLE_DEBUG_MODE) {
            log.info(creepCount + ' creeps found in the playground.');
        }

        CreepManager._buildMissingCreeps(room, creeps);

        _.each(creeps, (creep: Creep) => {
            if (creep.memory.role === 'harvester') {
                Harverter.run(creep);
            } else if (creep.memory.role === 'upgrader') {
                Upgrader.run(creep);
            } else if (creep.memory.role === 'builder') {
                Builder.run(creep);
            }
        });
    }

    /**
     * Creates a new creep if we still have enough space.
     *
     * @param {Room} room
     * @param creeps
     */
    public static _buildMissingCreeps(room: Room, creeps: Creep[]) {

        const spawns: Spawn[] = room.find<Spawn>(FIND_MY_SPAWNS, {
            filter: (spawn: Spawn) => {
                return spawn.spawning === null;
            },
        });

        if (Config.ENABLE_DEBUG_MODE) {
            if (spawns[0]) {
                log.info('Spawn: ' + spawns[0].name);
            }
        }


        // Recovery mode
        if (!CreepManager.countByRole(creeps, 'harvester')) {
            for (let name in creeps) {
                Game.creeps[name].memory.role = 'harvester';
            }
        }

        // Spwan new
        if  (room.energyAvailable === room.energyCapacityAvailable) {
            let parts = [WORK, MOVE, CARRY];
            let cost = BODYPART_COST[WORK] + BODYPART_COST[MOVE] + BODYPART_COST[MOVE];
            let i = 0;
            while (true) {
                let part = i % 3 === 0 ? CARRY : i % 2 === 0 ? MOVE : WORK;
                if (cost + BODYPART_COST[part] > room.energyCapacityAvailable) {
                    break;
                } else {
                    parts.push(part);
                    cost += BODYPART_COST[part];
                }
                i++;
            }


            let maxId = 1;
            for (let name in Game.creeps) {
                if (Game.creeps[name].memory.id > maxId) {
                    maxId = Game.creeps[name].memory.id;
                }
            }

            if (CreepManager.countByRole(creeps, 'harvester') < 4) {
                spawns[0].createCreep(parts, (maxId + 1) + '-harvest', {id: (maxId + 1), role: 'harvester'});
            } else if (CreepManager.countByRole(creeps, 'upgrader') < 3) {
                spawns[0].createCreep(parts, (maxId + 1) + ' -upgrader', {id: (maxId + 1), role: 'upgrader'});
            } else if (CreepManager.countByRole(creeps, 'builder') < 3) {
                spawns[0].createCreep(parts, (maxId + 1) + '-builder', {id: (maxId + 1), role: 'builder'});
            }
        }
    }

    /**
     * Count creep by role
     * @param creeps
     * @param role
     * @returns {number}
     */
    public static  countByRole(creeps: Creep[], role: string) {
        let count = 0;
        _.each(creeps, (creep: Creep) => {
            if (creep.memory.role === role) {
                count++;
            }
        });
        return count;
    }
}

